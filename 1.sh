#!/usr/bin/env bash


if [ $# -eq 2 ]; then
    echo param 2
elif [ $# -eq 3 ]; then
    echo "param 3"
else
    cat <<< 'asdasd
        这是是多行的内容随便写的
    '
    echo '123213
        dsds
        4444
        1321
    '
fi
echo "sql_"$(date "+%Y%m%d")".log"

# $* 将所有的参数看做一个字符串
# $@ 将每个参数看做一个字符串
#模拟参数
set -- 11 22 33
for i in $*; do
    echo $i
done;

for i in $@; do
    echo $i
done;
for i in "$@"; do
    echo $i
done;
for i in "$*"; do  # 这种情况下一个字符串
    echo $i
done;
for i ; do  # 相当于 in "$@"
    echo $i
done;
show_baby() {
    case $1 in
        男 | M)
            echo "是位小少爷"
            ;;
        女 | F)
            echo "是位小千金"
            ;;
        *)
            echo "有没有搞错"
            ;;
    esac
}
show_baby ${1}
show_baby $2
while [ $# -eq 2 ]; do
    sleep 1
    echo $(date "+%Y-%m-%d %H:%M:%S")
done

