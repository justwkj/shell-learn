#!/usr/bin/env bash

#while 1+..+100
i=0
sum=0
while [ $i -le 100 ]
do
    ((sum=sum+i))
    ((i++))
done
echo $sum
