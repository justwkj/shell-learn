#!/usr/bin/env bash

## sed 后面使用单引号
#[root@www ~]# sed [-nefr] [动作]
#选项与参数：
#-n ：使用安静(silent)模式。在一般 sed 的用法中，所有来自 STDIN 的数据一般都会被列出到终端上。但如果加上 -n 参数后，则只有经过sed 特殊处理的那一行(或者动作)才会被列出来。
#-e ：直接在命令列模式上进行 sed 的动作编辑；
#-f ：直接将 sed 的动作写在一个文件内， -f filename 则可以运行 filename 内的 sed 动作；
#-r ：sed 的动作支持的是延伸型正规表示法的语法。(默认是基础正规表示法语法)
#-i ：直接修改读取的文件内容，而不是输出到终端。
#
#动作说明： [n1[,n2]]function
#n1, n2 ：不见得会存在，一般代表『选择进行动作的行数』，举例来说，如果我的动作是需要在 10 到 20 行之间进行的，则『 10,20[动作行为] 』
#
#function：
#a ：新增， a 的后面可以接字串，而这些字串会在新的一行出现(目前的下一行)～
#c ：取代， c 的后面可以接字串，这些字串可以取代 n1,n2 之间的行！
#d ：删除，因为是删除啊，所以 d 后面通常不接任何咚咚；
#i ：插入， i 的后面可以接字串，而这些字串会在新的一行出现(目前的上一行)；
#p ：列印，亦即将某个选择的数据印出。通常 p 会与参数 sed -n 一起运行～
#s ：取代，可以直接进行取代的工作哩！通常这个 s 的动作可以搭配正规表示法！例如 1,20s/old/new/g 就是啦！
nl /etc/passwd | sed -n '5,7p'
sed '/root/p'
nl /etc/passwd | sed -n '/root/{s/bash/blueshell/;p}'

#如果只替换/etc/passwd的第一个bash关键字为blueshell，就退出
nl /etc/passwd | sed -n '/bash/{s/bash/blueshell/;p;q}'


#除了整行的处理模式之外， sed 还可以用行为单位进行部分数据的搜寻并取代。基本上 sed 的搜寻与替代的与 vi 相当的类似！他有点像这样：
sed 's/要被取代的字串/新的字串/g'
/sbin/ifconfig eth0 | grep 'inet addr' | sed 's/^.*addr://g'

#查看文件去除空行
 sed '/^$/d' msg.txt |nl
 grep -v "^$" msg.txt |nl
