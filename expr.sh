#!/usr/bin/env bash

s=1
expr $s + 0 >/dev/null
[ $? -eq 0 ] && {
    echo 'is int'
}
a=1
b=1
if [ $a == $b ]; then
    echo 'eqal == '
fi

if [ $a -eq $b ]; then
    echo 'eqal -eq'
fi

null=''
if [ -n "$null" ]; then
    echo ${#null}
    echo '-n 长度不为0'
fi
if [ -z "$null" ]; then
    echo ${#null}
    echo '-z 长度为0'
fi
