#!/bin/bash
#auth: wkj
#date: 2019-01-19
#mail: justwkj@gmail.com
#func: auto deploy php and web

PHP_CODE_PATH='/app/wwwroot/buyticket'
PHP_GIT_BRANCH='dev_pc_buyticket'
WEB_CODE_PATH='/app/wwwroot/testsh/public'
WEB_CODE_BRANCH='dev_pc_buyticket'


usage(){
	echo "Usage: ${0} {-all|-a, -php, -web}"
	exit
}

[ "$#" != 1 ] &&  usage

updatePHP(){
	[ ! -d "$PHP_CODE_PATH" ] && {
		echo  $PHP_CODE_PATH "not exists"
		exit 
	}
	cd $PHP_CODE_PATH
	git add . -A && git stash
	git pull origin $PHP_GIT_BRANCH
}
updateWeb(){
	
	[ ! -d "$WEB_CODE_PATH" ] && {
		echo  $WEB_CODE_PATH "not exists"
		exit 
	}
	# to deploy web
}
case "$1" in
	-all|-a)
		echo php and web
	;;
	-php)
		updatePHP
	;;
	-web)
		echo web
		;;
	*)
		usage
	;;
esac
