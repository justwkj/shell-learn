#!/usr/bin/env bash

echo  "12113451245"|awk -v FS='1' '{print $0}'



#awk 'NR>19&&NR<31'  [file name]   显示20到30行
#
#awk 'NR==30'  [file name]    显示第30行

awk 'BEGIN{print "array("}{printf "\"%s\" => \"%s\",\n", $1,$10}END{print")"}' awk.txt

# 区别 一个是连续输出,一个是使用分隔符输出
awk -v FS="@@" -v OFS='##' '{print $1 $2}' log.txt
awk -v FS="@@" -v OFS='##' '{print $1,$2}' log.txt

grep sendCode xxx.txt > 123.log
awk -v FS='"' '{print $8}' 123.log  > ip.txt

awk -v FS='"' '{print $8}' 123.log  | sort $1|uniq

#uniq 去重
#sort -r 倒序

#查看文件 json格式化
tail -n 2 20170920_upload.log| head -n 2 |awk -v FS='\t' '{print $2}' | jq
