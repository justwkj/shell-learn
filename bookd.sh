#!/usr/bin/env bash
case "$#" in
    0)
        t=1
        ;;
    1)
        expr $1 + 0 &> /dev/null

        [ "$?" -eq 0 ] && {
            t=$1
        } || {
            echo "参数请输入整数"
            exit 1
        }
        ;;
    *)
        echo "Usage ${0} second"
        ;;
esac

 t=$1

 while true
 do
     if [ $(netstat  -a -n |grep '0.0.0.0:4000' |grep 'LISTENING' | wc -l) -eq 0 ]
         then
                 gitbook init >/dev/null 2>&1 && gitbook serve >/dev/null 2>&1
    fi
    sleep $t
 done


