#!/usr/bin/env bash
## seq - print a sequence of numbers 打印一些列的数值
#常用参数介绍:
#- w 等宽打印
#- s 指定输出分割符

### 例子

#打印1-100
seq 100


#-100到100 步长 2
seq -100 2 100

#1+2+3+....50
seq -s "+" 50

