#!/usr/bin/env bash

echo "current time is `date "+%Y-%m-%d %H:%M:%S"`"
t1=`date +%s`
echo "executing...."
sleep 10
t2=`date +%s`
echo "current time is `date "+%Y-%m-%d %H:%M:%S"`"
echo "time="$(($t2-$t1))
